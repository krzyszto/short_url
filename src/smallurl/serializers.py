from rest_framework import serializers
from rest_framework.reverse import reverse

from smallurl.models import ShortenedUrl
from project.settings import APP_DOMAIN


class ShortenedUrlSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShortenedUrl
        fields = ['long_url', 'alias', 'short_url']

    short_url = serializers.SerializerMethodField(read_only=True)

    def get_short_url(self, *args, **kwargs):
        alias = self.validated_data['alias']
        return APP_DOMAIN + reverse('alias_redirect_api_view', kwargs=dict(alias=alias))

    def to_internal_value(self, data):
        if not data.get('alias'):
            data['alias'] = ShortenedUrl.generate_alias()
        return super(ShortenedUrlSerializer, self).to_internal_value(data)
