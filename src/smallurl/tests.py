from unittest.mock import patch

from django.urls import reverse

from rest_framework import status
from rest_framework.test import APITestCase

from smallurl.models import ShortenedUrl


class ShortenedUrlTests(APITestCase):
    def tearDown(self) -> None:
        ShortenedUrl.objects.all().delete()

    @patch('smallurl.models.ShortenedUrl.generate_alias', return_value="test1234")
    def test_create_without_alias(self, *args):
        """
        Ensure we can create a new shortened url without alias, so it is generated.
        """
        url = reverse('create_shortened_url')
        data = {'long_url': 'http://example.com'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(ShortenedUrl.objects.count(), 1)
        self.assertEqual(ShortenedUrl.objects.get().long_url, 'http://example.com')
        self.assertEqual(ShortenedUrl.objects.get().alias, "test1234")

    def test_create_with_alias(self):
        url = reverse('create_shortened_url')
        data = {'long_url': 'http://example.com', 'alias': 'different_alias'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(ShortenedUrl.objects.count(), 1)
        self.assertEqual(ShortenedUrl.objects.get().long_url, 'http://example.com')
        self.assertEqual(ShortenedUrl.objects.get().alias, "different_alias")
