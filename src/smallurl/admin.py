from django.contrib import admin
from smallurl.models import ShortenedUrl


class ShortenedUrlAdmin(admin.ModelAdmin):
    pass


admin.site.register(ShortenedUrl, ShortenedUrlAdmin)
