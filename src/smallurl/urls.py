from django.urls import path

from smallurl.views import ShortenedUrlCreateAPIView, AliasRedirectAPIView

urlpatterns = [
    path('create/', ShortenedUrlCreateAPIView.as_view(), name='create_shortened_url'),
    path('alias/<alias>', AliasRedirectAPIView.as_view(), name='alias_redirect_api_view'),
]
