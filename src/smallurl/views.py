from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page

from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView, get_object_or_404

from smallurl.serializers import ShortenedUrlSerializer
from smallurl.models import ShortenedUrl


class ShortenedUrlCreateAPIView(CreateAPIView):
    queryset = ShortenedUrl.objects.all()
    serializer_class = ShortenedUrlSerializer
    permission_classes = [AllowAny, ]


class AliasRedirectAPIView(APIView):
    permission_classes = [AllowAny, ]

    @method_decorator(cache_page(60 * 60 * 2))
    def get(self, *args, **kwargs):
        shortened_url = get_object_or_404(ShortenedUrl, alias=kwargs['alias'])
        return redirect(shortened_url.long_url)
