import secrets
import string

from django.db import models

from project.settings import ALIAS_LENGTH


class ShortenedUrl(models.Model):
    long_url = models.URLField(blank=False, null=False)
    alias = models.CharField(max_length=64, blank=True, null=False, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)

    @staticmethod
    def generate_alias() -> str:
        return ''.join(secrets.choice(string.ascii_uppercase + string.digits) for _ in range(ALIAS_LENGTH))

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.alias:
            self.alias = self.generate_alias()

        return super(ShortenedUrl, self).save(
            force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields,
        )
