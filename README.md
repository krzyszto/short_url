# Installation
1. Requirements
   1. Use `python 3.10` 
   2. Install pip-tools `pip install pip-tools`
2. Django
   1. Copy `.env`
   2. Install requirements `pip-sync`
   3. Run migrations `./manage.py migrate`
   4. Create Superuser, if needed `./manage.py createsuperuser`
   5. Runserver `./manage.py runserver`

# Requirements
Always update `requirements.in`. There are two such files: `../requirements.in` that contains DEV requirements and `../src/requirements.in` that contains PROJECT requirements.

For both of them please use `pip-compile` command to update them. More info on pip-tools [HERE](https://github.com/jazzband/pip-tools/)
